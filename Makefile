up: build
	docker \
		run \
			--rm \
			-ti \
			--user=`id -u`:`id -g` \
			--volume=$(PWD):/usr/src/advent-of-code \
			--workdir=/usr/src/advent-of-code \
			tomtomsen/advent-of-code-2017 \
			sh

build: Dockerfile
	docker build --pull --tag tomtomsen/advent-of-code-2017 .

editorconfig-check:
	docker \
		run \
			--rm \
			--user=`id -u`:`id -g` \
			--volume=$(PWD):/check \
			--volume=/dev/null:/usr/bin/git \
			--workdir=/check \
			mstruebing/editorconfig-checker \
				/ec \
				-exclude "\.git/|target/"
