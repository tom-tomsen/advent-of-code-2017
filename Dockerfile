FROM rust:1-alpine

WORKDIR /usr/src/advent-of-code
COPY . .

# RUN cargo install --path .

CMD ["advent-of-code"]
